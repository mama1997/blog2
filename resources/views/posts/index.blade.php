<!DOCTYPE html>
<html lang="en">
   <head>
        <meta charset="utf-8">
         
   <title>visual thoughts</title>
     <!-- Hoja de estilos de Bootstrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

            <link rel="stylesheet" type="text/css" href="custom.css">
      
</head>



@if(Session::has('exito'))
<div style="background: lightgreen; color: #fff;">
	<p>{{ Session::get('exito') }}</p>
</div>
@endif

@if(Session::has('info'))
<div style="background: lightblue; color: #fff;">
	<p>{{ Session::get('info') }}</p>
</div>
@endif

<body>

<section class="home">
<center>
<form method="POST" action="{{route('blog.store')}}">
    {{ csrf_field() }}
  <div class="form-row">
    <div class="form-group col-md-6">
   <label>Titulo</label><br>
    <input type="text" name="title" required=""><br>
    </div>
    <div class="form-group col-md-6">
     <label>Cuerpo</label><br>
    <textarea name="body" required="" rows="10"></textarea><br>
    </div>
  </div>
  <div class="form-group">
   <label>Autor</label><br>
    <input type="text" name="author"><br>
  </div>

  <div class="form-group">
     <label>Fecha</label><br>
    <input type="date" name="date"><br>
  </div>

  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Palabras clave</label><br>
    <input type="text" name="keywords"><br>
    </div>
 
  <button type="submit">Guardar datos</button>
   
</form></center>
</section>

  <section class="portafolio">
        <div class="container">
            <div class="row">
                 @foreach($posts as $post)
                <div class="col-md-3">
                    <div class="card">
                        <img src="https://source.unsplash.com/800x600/?nature,grass" class="card-img-top">
                        <div class="card-body">
                 
                        <h3>{{ $post->title }}</h3>
                        <p>{{ $post->body }}</p>
                        <form method="POST" action="{{ route('blog.destroy', $post->id)}}">
                        <button type="submit">Borrar</button>
                     {{ csrf_field() }}
                  {{ method_field('DELETE')}}
                  </form>
                 
                           </div>
                        </div>
                    </div> 
                    @endforeach
            </div>
        </div>
    </section>



 <!-- Scripts de Bootstrap -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>


</body>
</html>