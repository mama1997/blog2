<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Info</title>
</head>
<body>

	<h1>Our team</h1>

	<h3>Nombre: {{ $nombre }}</h3>
	<h4>Telefono: {{ $telefono }}</h4>
	<h4>Email: {{ $email }}</h4>
	
	<h2>
		<a href="{{ route('index') }}">
			Regresar
		</a>
	</h2>

</body>
</html>