<?php

namespace App\Http\Controllers;


use Session;

use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index (){

          $posts = Post::all();


        return view('index')->with('posts', $posts);
    }

    public function InformacionDeContacto (){
        $nombre = 'Josue';
        $telefono = '477 555 555';
        $email = 'josuemaya@gmail';
    

        return view ('informacion')
        ->with('nombre',$nombre)
        ->with('telefono',$telefono)
        ->with('email',$email);
      
    }

      public function store(Request $request)
    {
        $post = new Post;

        $post->title = $request->title;
        $post->body = $request->body;
        $post->author = $request->author;
        $post->date = $request->date;
        $post->keywords = $request->keywords;

        $post->save();

        Session::flash('exito','Tu info se guardo exitosamente');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
         return view('posts.show');
    }
}