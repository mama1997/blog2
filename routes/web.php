<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index' )->name('index');
Route::get('josue', 'HomeController@InformacionDeContacto')->name('informacion');

/*
// CREATE / CREAR 
Route::post();
// READ / LEER 
Route::get();
// UPDATE / ACTUALIZAR
Route::put();
// DELATE / BORRA
Route::delete();
*/

Route::resource('/blog','PostController');


/*
blog.index
blog.store
blog.create
blog.show
blog.destroy 
*/